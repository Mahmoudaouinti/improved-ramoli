	package ramoli.moteur;

	import java.util.Vector;

import ramoli.connaissances.*;

	public abstract class MoteurInference {

		protected Projet projet;
		protected String trace;
		protected String nvFaits;

		public MoteurInference() {
		}

		public MoteurInference(Projet p) {
			projet = p;
		}

		public void setProjet(Projet p) {
			projet = p;
		}

		public Projet getProjet() {
			return projet;
		}

		public String getTrace() {
			return trace;
		}

		public void setTrace(String trace) {
			this.trace = trace;
		}
		@SuppressWarnings("unchecked")
		public String chainageAvant() {
			trace = new String();
			Vector<Regle> br = (Vector<Regle>) projet.getBR().clone();// copie
																		// de la
																		// BR
			Vector<Regle> ra = filtrage(br);// ensemble des regles applicables
			Regle r;
			Proposition p;
			while (ra.size() != 0) {
				r = selection(ra);
				p = execution(r);
				majConnaissances(r, p, br);
				ra = filtrage(br);
			}
			if (trace.length() != 0)
				return "Chainage avant effectu� avec succ�s.\n Trace du raisonnement :\n"
						+ trace;
			return "Chainage avant effectu� avec succ�s.\n Aucun fait d�duis.";
		}

		/*
		 * sauv avec trace simple public String chainageAvant(){ trace = new
		 * String(); Vector<Regle> br =
		 * (Vector<Regle>)projet.getBR().clone();//copie de la BR Vector<Regle>
		 * ra = filtrage(br);//ensemble des regles applicables Regle r;
		 * Proposition p; while(ra.size()!=0){ r = selection(ra); p =
		 * execution(r); trace +=majConnaissances(r,p,br); ra = filtrage(br); }
		 * if(trace.length()!=0) return
		 * "Chainage avant effectu� avec succ�s.\n Modifications dans la base des faits :\n"
		 * +trace; return
		 * "Chainage avant effectu� avec succ�s.\n Aucun fait d�duis."; }
		 */

		/*
		 * determine l'ensemble des regles declanchables dans la BR temporelle
		 */
		private Vector<Regle> filtrage(Vector<Regle> br) {
			Vector<Regle> res = new Vector<Regle>();
			// parcourir les regles pour trouver celles qui ont declechables
				for (int i = 0; i < br.size(); i++) {
						if (verifPremisse(br.get(i)))
						
						res.add(br.get(i));
									
					}
				
			return res; 
		}
		
		
		/*
		 * verifie si la premisse d'une regle est verifi�e
		 */
		private boolean verifPremisse(Regle r) {
			boolean ok = true;
			int i = 0;
			while (i < r.getPremisse().size() && ok) {
				ok = verifElement(r.getPremisse().get(i));
				i++;
			}
			return ok;
		}

		/*
		 * verifie si un element de la premisse de la regle existe dans la BF
		 */
		public abstract boolean verifElement(Proposition p);

		/*
		 * selectionne une regle � partir de l'agenda selon une strategie de
		 * resolution de conflits
		 */
		public Regle selection(Vector<Regle> ra) {
			Regle r = ra.get(0);
			ra.remove(0);
			return r;
		}

		public abstract Proposition execution(Regle r);

		public abstract void majConnaissances(Regle r, Proposition p,
				Vector<Regle> br);

	}
