	package ramoli.moteur;

	import java.util.Vector;

import ramoli.connaissances.*;
import ramoli.modificateurs.*;
import ramoli.util.Connecteurs;
import ramoli.util.Operation;

	public class MoteurApproximatif extends MoteurInference {

		public MoteurApproximatif() {
			super();
		}

		public MoteurApproximatif(Projet p) {
			super(p);
		}

		public boolean verifElement(Proposition p) {
			int i = 0;
			boolean trouv = false;
			Fait f;
			while (i < projet.getBF().size() && !trouv) {
				f = projet.getBF().get(i);
				if (p.getVariable().equals(f.getVariable()));
						
					trouv = true;
				i++; }
			return trouv;
		}

		public Proposition execution(Regle r) {
			Vector<Fait> observation = determineObservation(r.getPremisse());
			
			// maj de la trace
			trace += "\n R�gle " + r;
			trace += "\n Observation " + observation.get(0);
			for (int i = 1; i < observation.size(); i++)
				trace += " ET " + observation.get(i);
			
			
			//Calcul de similarit� entre premisse et observation
			String nomPremisse = r.getPremisse().get(0).getMultiEnsemble().getPredicat().getNom();
			String nomObservation = observation.get(0).getMultiEnsemble().getPredicat().getNom();
			int M = r.getConclusion().getMultiEnsemble().getPredicat().getBase().getTaille();
			int sim = similarite(nomPremisse,nomObservation,M);

			//Calcul du degr� du r�sultat
			int alpha = r.getConclusion().getMultiEnsemble().getDegre();
			int beta = observation.get(0).getMultiEnsemble().getDegre();
			String tnorm = this.projet.getTnorm();
			int gamma = Connecteurs.tnorm(Connecteurs.tnorm(sim,beta,tnorm,M), alpha, tnorm, M);
			Proposition conclusion = r.getConclusion();
			conclusion.getMultiEnsemble().setDegre(gamma);
				
			
			
		// New features to calculate approximative rule similarity
		conclusion.getMultiEnsemble().getPredicat().setBase
		(
			r.getConclusion().getMultiEnsemble().getPredicat().getBase()
		)	; 
		trace += "\n------> Conclusion : " + conclusion + "\n";
			
		//Size of base M (Overriding space collapsce) 
		int M1 = Operation.leastCommonMultipleBase
		(
			r.getPremisse(), r.getConclusion(), observation
		)	;
		
		// Knowledge interfacing		
		Vector<Proposition> premisse = MSG.interfacage(r.getPremisse(), M1);
		Proposition conclusion_modificators = MSG.interfacage(r.getConclusion(), M1);
		Vector<Proposition> observation2 = MSG.interfacage(
		convert(observation), M1);
		// mod determination
		Vector<MSG> mods = determineMSGs(premisse, observation2);
		MSG mod;
		if (r.getJonction() == 'c')
			mod = MSG.mnorm(mods, this.projet.getTnorm(), M1);
		else
			mod = MSG.mconorm(mods, this.projet.getTconorm(), M1);
		// Conclusion Computing and de-interfacing 
		conclusion_modificators.setMultiEnsemble(mod.trans(conclusion.getMultiEnsemble()));
		conclusion_modificators = MSG.desinterfacage
		(
			conclusion_modificators
		, 	r.getConclusion().getMultiEnsemble().getPredicat().getBase().getTaille()
		
		)	; 
					
		// restore conclusion base with rule base with same size data modeling
		
		conclusion.getMultiEnsemble().getPredicat().setBase
		(
			r.getConclusion().getMultiEnsemble().getPredicat().getBase()
		)   ; 
		trace += "\n------> Conclusion : " + conclusion_modificators + "\n"; 
		 
			return conclusion;   		
		}

		/*
		  Overridng benchmarking computing and use NLTK/NLP python boost in JAVA 
		 */ 
		
		public double similarite7(String nomPremisse, String nomObservation, int m) {
		
			double x_ramoli=RamoliSimilarity.compute7(nomPremisse, nomObservation);
		
			return x_ramoli;	
		}
		

		/*
		  Rule execution Binding
		 */
		private Vector<Fait> determineObservation ( Vector<Proposition> premisse) {
			Vector<Fait> res = new Vector<Fait>();
			for (int i = 0; i < premisse.size(); i++) {
				res.add(determineObservation(premisse.get(i)));
			}
			return res;
		}

		private Fait determineObservation(Proposition elemPre) 
		{
			int j = 0;
			boolean trouv = false;
			Fait fait = null;
			while (j < projet.getBF().size() && !trouv) 
			{
				fait = projet.getBF().get(j);
				if (fait.getVariable().equals(elemPre.getVariable()))
						
					trouv = true;
				else
					j++;
			}
			if (trouv)
				return fait;
			else
				return null;
		}
		/*
		 Number of modificators , escaping area 
		 */
		private Vector<MSG> determineMSGs(Vector<Proposition> premisse,
				Vector<Proposition> observation) 
		{
			Vector<MSG> res = new Vector<MSG>();
			for (int i = 0; i < premisse.size(); i++) 
			{
			int degreP = premisse.get(i).getMultiEnsemble().getDegre();
			int degreO = observation.get(i).getMultiEnsemble().getDegre();
			MSG mod;
			if (degreP == degreO)
				mod = new CC();
			else if (degreP > degreO)
				mod = new CW(degreP - degreO);
			else
				mod = new CR(degreO - degreP);
			res.add(mod);
			} 
			return res;
		}

		public void majConnaissances(Regle r, Proposition p, Vector<Regle> br) 
		{
			String res = projet.addFait(p);
			br.remove(r);
			trace = trace + "\n" + res + "\n";
		}

		private Vector<Proposition> convert(Vector<Fait> v) 
		{
			Vector<Proposition> res = new Vector<Proposition>();
			for (int i = 0; i < v.size(); i++) 
			{
				res.add((Proposition) v.get(i).clone());
			}
			return res;
		}

	}
