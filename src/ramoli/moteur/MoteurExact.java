package ramoli.moteur; 
import java.util.Vector;

import ramoli.connaissances.*;

public class MoteurExact extends MoteurInference {
	
	public MoteurExact(Projet p){
		super(p);
	}
	
	public boolean verifElement(Proposition p){
		int i = 0;
		boolean trouv = false;
		Fait f;
		while(i<projet.getBF().size() && !trouv){
			f = projet.getBF().get(i);
			if(p.getVariable().equals(f.getVariable()) && 
					p.getMultiEnsemble().getPredicat().getNom().equals(f.getMultiEnsemble().getPredicat().getNom()) &&
					p.getMultiEnsemble().getDegre() == f.getMultiEnsemble().getDegre())
				trouv = true;
			i++;
		}
		return trouv;
	}
	
	public Proposition execution(Regle r){
		return r.getConclusion();
	}
	
	public void majConnaissances(Regle r,Proposition p,Vector<Regle> br){
		String res = projet.addFait(p);
		br.remove(r);
		trace = trace + res+"\n";
	}
	
}