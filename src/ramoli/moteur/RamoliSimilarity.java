package ramoli.moteur;

import java.util.Vector;

import ramoli.connaissances.Base;
import ramoli.connaissances.MultiEnsemble;
import ramoli.connaissances.Predicat;
import ramoli.connaissances.Proposition;
import ramoli.modificateurs.DC;
import ramoli.modificateurs.EC;
import ramoli.modificateurs.MSG;
import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.HirstStOnge;
import edu.cmu.lti.ws4j.impl.JiangConrath;
import edu.cmu.lti.ws4j.impl.LeacockChodorow;
import edu.cmu.lti.ws4j.impl.Lesk;
import edu.cmu.lti.ws4j.impl.Lin;
import edu.cmu.lti.ws4j.impl.Path;
import edu.cmu.lti.ws4j.impl.Resnik;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;

public abstract class RamoliSimilarity {

	private static ILexicalDatabase db = new NictWordNet();

	private static RelatednessCalculator[] rcs = { new HirstStOnge(db),
			new LeacockChodorow(db), new Lesk(db), new WuPalmer(db),
			new Resnik(db), new JiangConrath(db), new Lin(db), new Path(db) };

	public static int compute(String word1, String word2) {
		WS4JConfiguration.getInstance().setMFS(true);

		double s = new HirstStOnge(db).calcRelatednessOfWords(word1, word2);

		return (int) s;

	}

	public static double compute7(String word1, String word2) {
		WS4JConfiguration.getInstance().setMFS(true);

		double s7 = new Path(db).calcRelatednessOfWords(word1, word2);

		return s7;

	}

