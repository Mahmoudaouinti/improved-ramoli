package ramoli.util;

public class Connecteurs {
	
	public static int TG(int x, int y, int M)
	  {return Math.min(x,y);}
	
	public static int SG(int x, int y, int M)
	  {return Math.max(x,y);}
	
	public static int TL(int x, int y, int M)
	  {return Math.max(x+y-M+1,0);}
	
	public static int SL(int x, int y, int M)
	  {return Math.min(x+y,M-1);}
	
	public static int tnorm(int x,int y,String tnorm,int M){
		if(tnorm.equalsIgnoreCase("TG")) return TG(x,y,M);
		else if (tnorm.equalsIgnoreCase("TL")) return TL(x,y,M);
		else {
			System.out.println("erreur d'identification de la T-norme.");
			return 0;
		}
	}
	
	public static int tconorm(int x,int y,String tconorm,int M){
		if(tconorm.equalsIgnoreCase("SG")) return SG(x,y,M);
		else if (tconorm.equalsIgnoreCase("SL")) return SL(x,y,M);
		else {
			System.out.println("erreur d'identification de la T-conorme.");
			return 0;
		}
	}
	
	public static int dualTnorm(int x,int y,String tnorm,int M){
		if(tnorm.equalsIgnoreCase("TG")) return neg(TG(neg(x,M),neg(y,M),M),M);
		else if (tnorm.equalsIgnoreCase("TL")) return neg(TL(neg(x,M),neg(y,M),M),M);
		else {
			System.out.println("erreur d'identification de la T-norme.");
			return 0;
		}
	}
	
	public static int dualTconorm(int x,int y,String tconorm,int M){
		if(tconorm.equalsIgnoreCase("SG")) return neg(SG(neg(x,M),neg(y,M),M),M);
		else if (tconorm.equalsIgnoreCase("SL")) return neg(SG(neg(x,M),neg(y,M),M),M);
		else {
			System.out.println("erreur d'identification de la T-conorme.");
			return 0;
		}
	}
	
	public static int neg(int x, int M){
		return M-1-x;
	}
	
	

}
