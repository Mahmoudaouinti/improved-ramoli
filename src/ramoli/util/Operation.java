package ramoli.util;

import java.util.Vector;
import ramoli.connaissances.*;

public class Operation {
	
	public static int leastCommonMultiple(int a,int b){
		int n;
	    for(n=1;;n++)
	    {
	  	if(n%a == 0 && n%b == 0)
	  	  return n;
	    }
	}

	public static int leastCommonMultipleBase(Vector<Proposition> premisse,Proposition conclusion,Vector<Fait> observation){
		int res = premisse.get(0).getMultiEnsemble().getPredicat().getBase().getTaille();
		for(int i=0;i<premisse.size();i++)
			res = leastCommonMultiple(res-1,premisse.get(i).getMultiEnsemble().getPredicat().getBase().getTaille()-1)+1;
		res = leastCommonMultiple(res-1,conclusion.getMultiEnsemble().getPredicat().getBase().getTaille()-1)+1;
		for(int i=0;i<observation.size();i++)
			res = leastCommonMultiple(res-1,observation.get(i).getMultiEnsemble().getPredicat().getBase().getTaille()-1)+1;
		return res;
	}

    
}
