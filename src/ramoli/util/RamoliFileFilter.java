package ramoli.util;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class RamoliFileFilter extends FileFilter {

	@Override
	public boolean accept(File f) {
		if(f.isDirectory()) return true;
		else if(f.getName().endsWith(".ramoli")) return true;
			else return false;
	}

	@Override
	public String getDescription() {
		return "Projet RAMOLI";
	}

}
