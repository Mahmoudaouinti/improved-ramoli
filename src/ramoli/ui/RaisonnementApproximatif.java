package ramoli.ui;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import java.awt.Toolkit;
import java.awt.GridLayout;
import java.awt.GridBagLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;

import java.awt.Rectangle;
import java.awt.Dimension;

import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JTextArea;

import ramoli.moteur.MoteurApproximatif;
import ramoli.moteur.RamoliSimilarity;

public class RaisonnementApproximatif extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JLabel jLabel = null;
	private JLabel jLabel1 = null;
	private JButton btn_fermer = null;
	private JTextArea tar_trace = null;
	
	/**
	 * This is the default constructor
	 */
	public RaisonnementApproximatif() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(403, 308);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("D:/These/RAMOLI/RAMOLI3/src/icon.png"));
		this.setContentPane(getJContentPane());
		this.setTitle("Raisonnement approximatif");
		this.setVisible(true);
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jLabel1 = new JLabel();
			jLabel1.setBounds(new Rectangle(53, 39, 160, 16));

            jLabel1.setText("Trace du raisonnement :");
			
            
           jLabel = new JLabel();
			jLabel.setBounds(new Rectangle(53, 15, 217, 16));
			jLabel.setText("Chainage avant realise avec succés.");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(jLabel, null);
			jContentPane.add(jLabel1, null);
			jContentPane.add(getBtn_fermer(), null);
			jContentPane.add(getTar_trace(), null);
		}
		return jContentPane;
	}

	/**
	 * This method initializes btn_fermer	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_fermer() {
		if (btn_fermer == null) {
			btn_fermer = new JButton();
			btn_fermer.setBounds(new Rectangle(144, 227, 96, 25));
			btn_fermer.setText("Fermer");
			btn_fermer.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					dispose();
				}
			});
		}
		return btn_fermer;
	}

	/**
	 * This method initializes tar_trace	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	 private JTextArea getTar_trace() {
		if (tar_trace == null) {
			tar_trace = new JTextArea();
			tar_trace.setBounds(new Rectangle(19, 69, 347, 142));
			
			
			MoteurApproximatif moteur = new MoteurApproximatif();
			double x = moteur.similarite7("rouge","orange",10);
			tar_trace.setText("The Similarity Hash Value is : "+x);
			RamoliSimilarity rs = new RamoliSimilarity();
			double  ramoli_pre = rs.compute("rouge", "orange",1);
			tar_trace.setText("Effective cell similar to : ", ramoli_pre);
			
		}
		return tar_trace;
	} 
	 
	 
	 
		/**
		 * @param args
		 */
		public static void main(String[] args) {
			// TODO Auto-generated method stub
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					RaisonnementApproximatif thisClass = new RaisonnementApproximatif();
					thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					thisClass.setVisible(true);
				}
			});
		}

}  //  @jve:decl-index=0:visual-constraint="10,10"
