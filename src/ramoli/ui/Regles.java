package ramoli.ui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JFrame;

import java.awt.Dimension;

import javax.swing.JLabel;

import java.awt.Rectangle;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import ramoli.connaissances.*;

import javax.swing.JRadioButton;

import ramoli.connaissances.Proposition;
import ramoli.connaissances.Regle;

import java.awt.Toolkit;

public class Regles extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JComboBox cmb_variable = null;
	private JLabel jLabel1 = null;
	 private JComboBox cmb_degre = null;
	private JComboBox cmb_predicat = null;
	private JList lst_premisse = null;
	private JLabel jLabel2 = null;
	private JButton btn_ajout_premisse = null;
	private JLabel jLabel = null;
	private JComboBox cmb_variable1 = null;
	private JLabel jLabel11 = null;
    private JComboBox cmb_degre1 = null;
	private JComboBox cmb_predicat1 = null;
	private JButton btn_ajouter = null;
	public static JList lst_regles = null;
	private JButton btn_valider = null;
	private JButton btn_annuler = null;
	private Vector<Regle> regles;  //  @jve:decl-index=0:
	private DefaultListModel model_pre = null;
	private DefaultListModel model = null;
	private Vector<Proposition> premisse = new Vector<Proposition>();
	private JRadioButton rdn_conjonction = null;
	private JRadioButton rdn_disjonction = null;
	private JLabel jLabel3 = null;

	/**
	 * This is the default constructor
	 */
	public Regles() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(586, 570);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("D:/These/RAMOLI/RAMOLI3/src/icon.png"));
		this.setContentPane(getJContentPane());
		this.setTitle("R�gles");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jLabel3 = new JLabel();
			jLabel3.setBounds(new Rectangle(46, 311, 179, 16));
			jLabel3.setText("Liste des r�gles :");
			jLabel11 = new JLabel();
			jLabel11.setBounds(new Rectangle(214, 216, 18, 16));
			jLabel11.setText("est");
			jLabel = new JLabel();
			jLabel.setBounds(new Rectangle(19, 213, 38, 16));
			jLabel.setText("alors");
			jLabel2 = new JLabel();
			jLabel2.setBounds(new Rectangle(27, 119, 38, 16));
			jLabel2.setText("Si");
			jLabel1 = new JLabel();
			jLabel1.setBounds(new Rectangle(198, 29, 38, 16));
			jLabel1.setText("est");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(getCmb_variable(), null);
			jContentPane.add(jLabel1, null);
			jContentPane.add(getCmb_degre(), null);
			jContentPane.add(getCmb_predicat(), null);
			jContentPane.add(getLst_premisse(), null);
			jContentPane.add(jLabel2, null);
			jContentPane.add(getBtn_ajout_premisse(), null);
			jContentPane.add(jLabel, null);
			jContentPane.add(getCmb_variable1(), null);
			jContentPane.add(jLabel11, null);
			jContentPane.add(getCmb_degre1(), null);
			jContentPane.add(getCmb_predicat1(), null);
			jContentPane.add(getBtn_ajouter(), null);
			jContentPane.add(getLst_regles(), null);
			jContentPane.add(getBtn_valider(), null);
			jContentPane.add(getBtn_annuler(), null);
			jContentPane.add(getRdn_conjonction(), null);
			jContentPane.add(getRdn_disjonction(), null);
			jContentPane.add(jLabel3, null);
			
			//chargement des variables dans cmb_variable et cmb_variable1
			cmb_variable.addItem("");
			cmb_variable1.addItem("");
			for(int i=0;i<Menu.projet.getVariables().size();i++){
				cmb_variable.addItem(Menu.projet.getVariables().elementAt(i));
				cmb_variable1.addItem(Menu.projet.getVariables().elementAt(i));
			}
			
			//chargement des predicats dans cmb_predicat
			cmb_predicat.addItem("");
			cmb_predicat1.addItem("");
			for(int i=0;i<Menu.projet.getPredicats().size();i++){
				cmb_predicat.addItem(Menu.projet.getPredicats().elementAt(i).getNom());
				cmb_predicat1.addItem(Menu.projet.getPredicats().elementAt(i).getNom());
			}
			
			//chargement des regles deja enregistr�s
			regles = (Vector<Regle>)Menu.projet.getBR().clone();
			model = (DefaultListModel) lst_regles.getModel();
			for(int i=0;i<regles.size();i++)
				model.addElement(regles.elementAt(i));
			
			model_pre = (DefaultListModel) lst_premisse.getModel();
		}
		return jContentPane;
	}

	/**
	 * This method initializes cmb_variable	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getCmb_variable() {
		if (cmb_variable == null) {
			cmb_variable = new JComboBox();
			cmb_variable.setBounds(new Rectangle(36, 24, 143, 25));
		}
		return cmb_variable;
	}

	/**
	 * This method initializes cmb_degre	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getCmb_degre() {
		if (cmb_degre == null) {
			cmb_degre = new JComboBox();
			cmb_degre.setBounds(new Rectangle(251, 27, 134, 25));
		}
		return cmb_degre;
	} 

	/**
	 * This method initializes cmb_predicat	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getCmb_predicat() {
		if (cmb_predicat == null) {
			cmb_predicat = new JComboBox();
			cmb_predicat.setBounds(new Rectangle(394, 27, 125, 25));
			cmb_predicat.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
					Vector<String> degres = Menu.projet.getDegres(cmb_predicat.getSelectedItem().toString());
					cmb_degre.removeAllItems();
					for(int i = 0;i<degres.size();i++)
						cmb_degre.addItem(degres.elementAt(i));
				}
			});
		}
		return cmb_predicat;
	}

	/**
	 * This method initializes lst_premisse	
	 * 	
	 * @return javax.swing.JList	
	 */
	public JList getLst_premisse() {
		if (lst_premisse == null) {
			lst_premisse = new JList(new DefaultListModel());
			lst_premisse.setBounds(new Rectangle(78, 114, 324, 75));
		}
		return lst_premisse;
	}

	/**
	 * This method initializes btn_ajout_premisse	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_ajout_premisse() {
		if (btn_ajout_premisse == null) {
			btn_ajout_premisse = new JButton();
			btn_ajout_premisse.setBounds(new Rectangle(171, 68, 236, 31));
			btn_ajout_premisse.setText("Ajouter proposition � la premisse");
			btn_ajout_premisse.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if(cmb_variable.getSelectedItem().toString().equals(""))
						JOptionPane.showMessageDialog(null,"Veuillez choisir une variable.");
					else if(cmb_predicat.getSelectedItem().toString().equals(""))
						JOptionPane.showMessageDialog(null,"Veuillez choisir un predicat.");
					else if(cmb_degre.getSelectedItem().toString().equals(""))
						JOptionPane.showMessageDialog(null,"Veuillez choisir un degr�.");
					else {
						Proposition proposition = new Proposition(cmb_variable.getSelectedItem().toString() ,
								cmb_degre.getSelectedIndex(),
								cmb_predicat.getSelectedItem().toString());
						premisse.addElement(proposition);
						model_pre.addElement(proposition);
						cmb_variable.setSelectedIndex(0);
						cmb_predicat.setSelectedIndex(0);
					}
				}
			});
		}
		return btn_ajout_premisse;
	}

	/**
	 * This method initializes cmb_variable1	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getCmb_variable1() {
		if (cmb_variable1 == null) {
			cmb_variable1 = new JComboBox();
			cmb_variable1.setBounds(new Rectangle(70, 210, 127, 25));
		}
		return cmb_variable1;
	}

	/**
	 * This method initializes cmb_degre1	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getCmb_degre1() {
		if (cmb_degre1 == null) {
			cmb_degre1 = new JComboBox();
			cmb_degre1.setBounds(new Rectangle(251, 211, 134, 25));
		}
		return cmb_degre1;
	} 

	/**
	 * This method initializes cmb_predicat1	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getCmb_predicat1() {
		if (cmb_predicat1 == null) {
			cmb_predicat1 = new JComboBox();
			cmb_predicat1.setBounds(new Rectangle(394, 210, 125, 25));
			cmb_predicat1.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
					Vector<String> degres = Menu.projet.getDegres(cmb_predicat1.getSelectedItem().toString());
					cmb_degre1.removeAllItems();
					for(int i = 0;i<degres.size();i++)
						cmb_degre1.addItem(degres.elementAt(i));
				}
			});
		}
		return cmb_predicat1;
	}

	/**
	 * This method initializes btn_ajouter	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_ajouter() {
		if (btn_ajouter == null) {
			btn_ajouter = new JButton();
			btn_ajouter.setBounds(new Rectangle(198, 254, 165, 30));
			btn_ajouter.setText("Ajouter r�gle");
			btn_ajouter.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if(premisse.size()==0)
						JOptionPane.showMessageDialog(null,"Veuillez constuire la pr�misse de la regle.");
					else if(cmb_variable1.getSelectedItem().toString().equals(""))
						JOptionPane.showMessageDialog(null,"Veuillez choisir une variable.");
					else if(cmb_predicat1.getSelectedItem().toString().equals(""))
						JOptionPane.showMessageDialog(null,"Veuillez choisir un predicat.");
					else if(cmb_degre1.getSelectedItem().toString().equals(""))
						JOptionPane.showMessageDialog(null,"Veuillez choisir un degr�.");
					else {
						Proposition conclusion = new Proposition(cmb_variable1.getSelectedItem().toString()
								,cmb_degre1.getSelectedIndex(), cmb_predicat1.getSelectedItem().toString());
						Regle regle = new Regle(premisse,conclusion);
						if(rdn_conjonction.isSelected()) regle.setJonction('c');
						else regle.setJonction('d');
						regles.addElement(regle);
						model.addElement(regle);
						premisse = new Vector<Proposition>();
						model_pre.clear();
						cmb_variable1.setSelectedIndex(0);
						cmb_predicat1.setSelectedIndex(0);
					}
				}
			});
		}
		return btn_ajouter;
	}

	/**
	 * This method initializes lst_regles	
	 * 	
	 * @return javax.swing.JList	
	 */
	static JList getLst_regles() {
		if (lst_regles == null) {
			lst_regles = new JList(new DefaultListModel());
			lst_regles.setBounds(new Rectangle(46, 340, 476, 112));
		}
		return lst_regles;
	}

	/**
	 * This method initializes btn_valider	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_valider() {
		if (btn_valider == null) {
			btn_valider = new JButton();
			btn_valider.setBounds(new Rectangle(125, 482, 102, 31));
			btn_valider.setText("Valider");
			btn_valider.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Menu.projet.setBR(regles);
					dispose();
				}
			});
		}
		return btn_valider;
	}

	/**
	 * This method initializes btn_annuler	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_annuler() {
		if (btn_annuler == null) {
			btn_annuler = new JButton();
			btn_annuler.setBounds(new Rectangle(316, 481, 105, 29));
			btn_annuler.setText("Annuler");
			btn_annuler.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					dispose(); 
				}
			});
		}
		return btn_annuler;
	}

	/**
	 * This method initializes rdn_conjonction	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getRdn_conjonction() {
		if (rdn_conjonction == null) {
			rdn_conjonction = new JRadioButton();
			rdn_conjonction.setBounds(new Rectangle(423, 117, 97, 21));
			rdn_conjonction.setText("Conjonction");
			rdn_conjonction.setSelected(true);
		}
		return rdn_conjonction;
	}

	/**
	 * This method initializes rdn_disjonction	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getRdn_disjonction() {
		if (rdn_disjonction == null) {
			rdn_disjonction = new JRadioButton();
			rdn_disjonction.setBounds(new Rectangle(424, 153, 92, 24));
			rdn_disjonction.setText("Disjonction");
		}
		return rdn_disjonction;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
