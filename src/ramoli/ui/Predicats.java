package ramoli.ui;

import javax.swing.SwingUtilities;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JFrame;

import java.awt.Dimension;

import javax.swing.JButton;

import java.awt.Rectangle;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;

import ramoli.connaissances.Predicat;

import java.util.Vector;

import ramoli.connaissances.*;

import java.awt.Toolkit;

public class Predicats extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JButton btn_ajouter = null;
	private JLabel jLabel = null;
	private JTextField edt_predicat = null;
	private JList lst_predicats = null;
	private JButton btn_valider = null;
	private JButton btn_annuler = null;
	private DefaultListModel model = null;
	private JLabel jLabel1 = null;
	private JComboBox cmb_base = null;
	private Vector<Predicat> predicats = new Vector<Predicat>();
	private JLabel jLabel2 = null;

	/**
	 * This method initializes btn_ajouter	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_ajouter() {
		if (btn_ajouter == null) {
			btn_ajouter = new JButton();
			btn_ajouter.setBounds(new Rectangle(331, 36, 75, 26));
			btn_ajouter.setText("Ajouter");
			btn_ajouter.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if(edt_predicat.getText().equals("")){
						JOptionPane.showMessageDialog(null,"Veuillez introduire le nom du predicat.");
						edt_predicat.requestFocus();
					}
					 else if(cmb_base.getSelectedItem().toString().equals("")){
						JOptionPane.showMessageDialog(null,"Veuillez choisir la base du pr�dicat.");
					}
					else if(existe(edt_predicat.getText())){
						JOptionPane.showMessageDialog(null, "Il existe d�j� un predicat qui s'appelle "+edt_predicat.getText()+".");
						edt_predicat.selectAll();
					}
					else{
						Predicat p = new Predicat(edt_predicat.getText() ,cmb_base.getSelectedItem().toString());
						predicats.addElement(p);
						model.addElement(p.toString());
						edt_predicat.setText("");
						cmb_base.setSelectedIndex(0);
						
						edt_predicat.requestFocus();
						Menu.projet.setPredicats(predicats);
					}
				}
			});
		}
		return btn_ajouter;
	}
	
	private boolean existe(String nom){
		int i = 0;
		while(i<predicats.size() && !predicats.get(i).getNom().equals(nom))
			i++;
		return !(i==predicats.size());
	} 

	/**
	 * This method initializes edt_predicat	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getEdt_predicat() {
		if (edt_predicat == null) {
			edt_predicat = new JTextField();
			edt_predicat.setBounds(new Rectangle(171, 38, 129, 20));
		}
		return edt_predicat;
	}

	/**
	 * This method initializes lst_predicats	
	 * 	
	 * @return javax.swing.JList	
	 */
	private JList getLst_predicats() {
		if (lst_predicats == null) {
			lst_predicats = new JList(new DefaultListModel());
			lst_predicats.setBounds(new Rectangle(32, 149, 208, 136));
		}
		return lst_predicats;
	}

	/**
	 * This method initializes btn_valider	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_valider() {
		if (btn_valider == null) {
			btn_valider = new JButton();
			btn_valider.setBounds(new Rectangle(331, 109, 74, 26));
			btn_valider.setText("Valider");
			btn_valider.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Menu.projet.setPredicats(predicats);
					dispose();
				}
			});
		}
		return btn_valider;
	}

	/**
	 * This method initializes btn_annuler	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_annuler() {
		if (btn_annuler == null) {
			btn_annuler = new JButton();
			btn_annuler.setBounds(new Rectangle(331, 172, 78, 26));
			btn_annuler.setText("Annuler");
			btn_annuler.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					dispose(); 
				}
			});
		}
		return btn_annuler;
	}

	/**
	 * This method initializes cmb_bases	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getCmb_base() {
		if (cmb_base == null) {
			cmb_base = new JComboBox();
			cmb_base.setBounds(new Rectangle(171, 81, 130, 25));
		}
		return cmb_base;
	} 

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Predicats thisClass = new Predicats();
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setVisible(true);
			}
		});
	}

	/**
	 * This is the default constructor
	 */
	public Predicats() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(464, 354);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("D:/These/RAMOLI/RAMOLI3/src/icon.png"));
		this.setContentPane(getJContentPane());
		this.setTitle("Predicats");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jLabel2 = new JLabel();
			jLabel2.setBounds(new Rectangle(32, 127, 174, 16));
			jLabel2.setText("Liste des pr�dicats :");
			jLabel1 = new JLabel();
			jLabel1.setBounds(new Rectangle(32, 88, 38, 16));
	//		jLabel1.setText("Base :");
			jLabel = new JLabel();
			jLabel.setBounds(new Rectangle(32, 41, 71, 16));
			jLabel.setText("Nom :");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(getBtn_ajouter(), null);
			jContentPane.add(jLabel, null);
			jContentPane.add(getEdt_predicat(), null);
			jContentPane.add(getLst_predicats(), null);
			jContentPane.add(getBtn_valider(), null);
			jContentPane.add(getBtn_annuler(), null);
			jContentPane.add(jLabel1, null);
			jContentPane.add(getCmb_base(), null);
			jContentPane.add(jLabel2, null);
			
			//chargement de la liste des predicat deja enregistr�s
			model = (DefaultListModel) lst_predicats.getModel();
			System.out.println("--model--"+Menu.projet.getPredicats().size());
			predicats = (Vector<Predicat>)Menu.projet.getPredicats().clone();
			for(int i=0;i<Menu.projet.getPredicats().size();i++)
				model.addElement(Menu.projet.getPredicats().elementAt(i));
			
			for(int i=0; i< Menu.projet.getBases().size(); i++)
				System.out.println("-->"+Menu.projet.getBases().get(i).getNom());
			
			//chargement des bases
			cmb_base.addItem("");
			for(int i=0;i<Menu.projet.getBases().size();i++)
				cmb_base.addItem(Menu.projet.getBases().elementAt(i).getNom());
			
		}
		return jContentPane; 
	} 

}  //  @jve:decl-index=0:visual-constraint="10,10"
