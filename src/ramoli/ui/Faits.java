package ramoli.ui;

import javax.swing.SwingUtilities;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JFrame;

import java.awt.Dimension;

import javax.swing.JLabel;

import java.awt.Rectangle;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;

import ramoli.connaissances.Fait;
import ramoli.connaissances.*;

import java.awt.Toolkit;


public class Faits extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JLabel jLabel = null;
	private JComboBox cmb_variable = null;
	private JComboBox cmb_degre = null;
	private JComboBox cmb_predicat = null;
	private JButton btn_ajouter = null;
	private JList lst_faits = null;
	private JButton btn_valider = null;
	private JButton btn_annuler = null;
	private Vector<Fait> faits;  //  @jve:decl-index=0:
	private DefaultListModel model = null;
	private JLabel jLabel1 = null;
	
	

	/**
	 * This method initializes cmb_variable	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getCmb_variable() {
		if (cmb_variable == null) {
			cmb_variable = new JComboBox();
			cmb_variable.setBounds(new Rectangle(14, 33, 127, 25));
		}
		return cmb_variable;
	}

	/**
	 * This method initializes cmb_degre	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getCmb_degre() {
		if (cmb_degre == null) {
			cmb_degre = new JComboBox();
			cmb_degre.setBounds(new Rectangle(194, 33, 103, 25));
		}
		return cmb_degre;
	} 

	/**
	 * This method initializes cmb_predicat	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getCmb_predicat() {
		if (cmb_predicat == null) {
			cmb_predicat = new JComboBox();
			cmb_predicat.setBounds(new Rectangle(311, 34, 116, 25));
			cmb_predicat.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
					Vector<String> degres = Menu.projet.getDegres(cmb_predicat.getSelectedItem().toString());
					cmb_degre.removeAllItems();
					for(int i = 0;i<degres.size();i++)
						cmb_degre.addItem(degres.elementAt(i));
				}
			});
		}
		return cmb_predicat;
	}

	/**
	 * This method initializes btn_ajouter	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_ajouter() {
		if (btn_ajouter == null) {
			btn_ajouter = new JButton();
			btn_ajouter.setBounds(new Rectangle(449, 29, 83, 28));
			btn_ajouter.setText("Ajouter");
			btn_ajouter.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if(cmb_variable.getSelectedItem().toString().equals(""))
						JOptionPane.showMessageDialog(null,"Veuillez choisir une variable.");
					else if(cmb_predicat.getSelectedItem().toString().equals(""))
						JOptionPane.showMessageDialog(null,"Veuillez choisir un predicat.");
					else if(cmb_degre.getSelectedItem().toString().equals(""))
						JOptionPane.showMessageDialog(null,"Veuillez choisir un degr�.");
					else {
						Proposition p = new Proposition(cmb_variable.getSelectedItem().toString(),
								cmb_degre.getSelectedIndex(), 
								cmb_predicat.getSelectedItem().toString());
						addFait(p);
						refrechList();
						cmb_variable.setSelectedIndex(0);
						cmb_predicat.setSelectedIndex(0);
					}
				}
			});
		}
		return btn_ajouter;
	}
	
	public void addFait(Proposition p){
		//verifier d'abord si le fait existe deja
		//s'il existe on prend celui qui a le plus grand degre
		int i = 0;
		boolean trouv = false;
		
		

			while(i<faits.size() && !trouv){
		
		 	if(faits.get(i).getVariable().equals(p.getVariable()) &&
					faits.get(i).getMultiEnsemble().getPredicat().getNom().equals(p.getMultiEnsemble().getPredicat().getNom()))
				if(faits.get(i).getMultiEnsemble().getDegre() < p.getMultiEnsemble().getDegre()){
					faits.get(i).getMultiEnsemble().setDegre(p.getMultiEnsemble().getDegre()); 
				trouv = true;
			}
			i++;
		}
		if(!trouv){
			Fait f = new Fait(p);
			faits.addElement(f);
		}
			
			
		}
	
	
	public void refrechList(){
		model.clear();
		for(int i=0;i<faits.size();i++)
			model.addElement(faits.elementAt(i));
	}

	/**
	 * This method initializes lst_faits	
	 * 	
	 * @return javax.swing.JList	
	 */
	private JList getLst_faits() {
		if (lst_faits == null) {
			lst_faits = new JList(new DefaultListModel());
			lst_faits.setBounds(new Rectangle(29, 112, 288, 116));
		}
		return lst_faits;
	}

	/**
	 * This method initializes btn_valider	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_valider() {
		if (btn_valider == null) {
			btn_valider = new JButton();
			btn_valider.setBounds(new Rectangle(449, 115, 83, 28));
			btn_valider.setText("Valider");
			btn_valider.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Menu.projet.setBF(faits);
					dispose(); 
				}
			});
		}
		return btn_valider;
	}

	/**
	 * This method initializes btn_annuler	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_annuler() {
		if (btn_annuler == null) {
			btn_annuler = new JButton();
			btn_annuler.setBounds(new Rectangle(448, 167, 83, 28));
			btn_annuler.setText("Annuler");
			btn_annuler.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					dispose(); 
				}
			});
		}
		return btn_annuler;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Faits thisClass = new Faits();
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setVisible(true);
			}
		});
	}

	/**
	 * This is the default constructor
	 */
	public Faits() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(567, 280);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("D:/These/RAMOLI/RAMOLI3/src/icon.png"));
		this.setContentPane(getJContentPane());
		this.setTitle("Faits");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jLabel1 = new JLabel();
			jLabel1.setBounds(new Rectangle(32, 84, 150, 16));
			jLabel1.setText("Liste des faits :");
			jLabel = new JLabel();
			jLabel.setBounds(new Rectangle(155, 37, 24, 16));
			jLabel.setText("est");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(jLabel, null);
			jContentPane.add(getCmb_variable(), null);
			jContentPane.add(getCmb_degre(), null);
			jContentPane.add(getCmb_predicat(), null);
			jContentPane.add(getBtn_ajouter(), null);
			jContentPane.add(getLst_faits(), null);
			jContentPane.add(getBtn_valider(), null);
			jContentPane.add(getBtn_annuler(), null);
			jContentPane.add(jLabel1, null);
			
			//chargement des variables dans cmb_variable
			cmb_variable.addItem("");
			for(int i=0;i<Menu.projet.getVariables().size();i++)
				cmb_variable.addItem(Menu.projet.getVariables().elementAt(i));
			
			//chargement des predicats dans cmb_predicat
			cmb_predicat.addItem("");
			for(int i=0;i<Menu.projet.getPredicats().size();i++)
				cmb_predicat.addItem(Menu.projet.getPredicats().elementAt(i).getNom());
			
			//chargement des faits deja enregistr�s
			faits = (Vector<Fait>)Menu.projet.getBF().clone();
			model = (DefaultListModel) lst_faits.getModel();
			for(int i=0;i<faits.size();i++)
				model.addElement(faits.elementAt(i));
		}
		return jContentPane;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
