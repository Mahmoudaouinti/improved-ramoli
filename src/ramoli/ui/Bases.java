package ramoli.ui;

import javax.swing.SwingUtilities;
import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Rectangle;
import javax.swing.JTextField;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import java.util.Vector;
import ramoli.connaissances.*;
import java.awt.Toolkit;

public class Bases extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JLabel jLabel = null;
	private JLabel jLabel1 = null;
	private JTextField edt_nom = null;
	private JTextField edt_taille = null;
	private JButton btn_ajouter = null;
	private JList lst_bases = null;
	private JButton btn_valider = null;
	private JButton btn_annuler = null;
	private DefaultListModel model = null;
	private Vector<Base> bases = new Vector<Base>();  //  @jve:decl-index=0:
	private JLabel jLabel2 = null;
	private JLabel jLabel3 = null;
	private JTextField edt_degres = null;

	/**
	 * This method initializes edt_nom	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getEdt_nom() {
		if (edt_nom == null) {
			edt_nom = new JTextField();
			edt_nom.setBounds(new Rectangle(108, 34, 85, 20));
		}
		return edt_nom;
	}

	/**
	 * This method initializes edt_taille	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getEdt_taille() {
		if (edt_taille == null) {
			edt_taille = new JTextField();
			edt_taille.setBounds(new Rectangle(107, 64, 85, 20));
		}
		return edt_taille;
	}

	/**
	 * This method initializes btn_ajouter	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_ajouter() {
		if (btn_ajouter == null) {
			btn_ajouter = new JButton();
			btn_ajouter.setBounds(new Rectangle(306, 41, 84, 25));
			btn_ajouter.setText("Ajouter");
			btn_ajouter.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if(edt_nom.getText().equals("")){
						JOptionPane.showMessageDialog(null,"Veuillez introduire le nom de la base.");
						edt_nom.requestFocus();
					}
					else if(edt_taille.getText().equals("")){
						JOptionPane.showMessageDialog(null,"Veuillez introduire la taille de la base.");
						edt_taille.requestFocus();
					}
					else if(existe(edt_nom.getText())){
						JOptionPane.showMessageDialog(null,"Il existe d�j� une base qui s'appelle "+edt_nom.getText()+".");
						edt_nom.selectAll();
					}
					else{
						try{
							Base b = new Base(edt_nom.getText(),Integer.parseInt(edt_taille.getText()));
							Vector<String> degres = extraitDegres(edt_degres.getText());
							if(degres.size()!=b.getTaille())
								JOptionPane.showMessageDialog(null,"Le nombre de degr�s symboliques n'est pas �gal � la taille de la base.");
							else{
								b.setEchelle(degres);
								bases.add(b);
								model.addElement(b.toString());
								edt_nom.setText("");
								edt_taille.setText("");
							}
						}
						catch(Exception er){
							JOptionPane.showMessageDialog(null,edt_taille.getText()+" n'est pas une taille de base valide.");
							edt_taille.requestFocus();
						}
					}
					edt_nom.requestFocus();
				}
			});
		}
		return btn_ajouter;
	}
	
	private boolean existe(String nom){
		int i = 0;
		while(i<bases.size() && !bases.get(i).getNom().equals(nom))
			i++;
		return !(i==bases.size());
	}
	
	private Vector<String> extraitDegres(String s){
		Vector<String> res = new Vector<String>();
		String tab[] = s.split(";");
		for(int i=0;i<tab.length;i++)
			res.add(tab[i].trim());
		return res;
	}

	/**
	 * This method initializes lst_bases	
	 * 	
	 * @return javax.swing.JList	
	 */
	private JList getLst_bases() {
		if (lst_bases == null) {
			lst_bases = new JList(new DefaultListModel());
			lst_bases.setBounds(new Rectangle(29, 194, 358, 96));
		}
		return lst_bases;
	}

	/**
	 * This method initializes btn_valider	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_valider() {
		if (btn_valider == null) {
			btn_valider = new JButton();
			btn_valider.setBounds(new Rectangle(303, 95, 87, 26));
			btn_valider.setText("Valider");
			btn_valider.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Menu.projet.setBases(bases);
					dispose(); 
				}
			});
		}
		return btn_valider;
	}

	/**
	 * This method initializes btn_annuler	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_annuler() {
		if (btn_annuler == null) {
			btn_annuler = new JButton();
			btn_annuler.setBounds(new Rectangle(306, 148, 83, 24));
			btn_annuler.setText("Annuler");
			btn_annuler.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					dispose(); 
				}
			});
		}
		return btn_annuler;
	}

	/**
	 * This method initializes edt_degres	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getEdt_degres() {
		if (edt_degres == null) {
			edt_degres = new JTextField();
			edt_degres.setBounds(new Rectangle(27, 127, 239, 33));
		}
		return edt_degres;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Bases thisClass = new Bases();
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setVisible(true);
			}
		});
	}

	/**
	 * This is the default constructor
	 */
	public Bases() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(428, 349);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("D:/These/RAMOLI/RAMOLI3/src/icon.png"));
		this.setContentPane(getJContentPane());
		this.setTitle("Bases d'�chelles");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jLabel3 = new JLabel();
			jLabel3.setBounds(new Rectangle(28, 101, 233, 16));
			jLabel3.setText("Degr�s symboliques (s�par�s par ';') :");
			jLabel2 = new JLabel();
			jLabel2.setBounds(new Rectangle(30, 171, 166, 16));
			jLabel2.setText("Listes des bases d'�chelles :");
			jLabel1 = new JLabel();
			jLabel1.setBounds(new Rectangle(26, 66, 54, 16));
			jLabel1.setText("Tailles :");
			jLabel = new JLabel();
			jLabel.setBounds(new Rectangle(27, 36, 38, 16));
			jLabel.setText("Nom :");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(jLabel, null);
			jContentPane.add(jLabel1, null);
			jContentPane.add(getEdt_nom(), null);
			jContentPane.add(getEdt_taille(), null);
			jContentPane.add(getBtn_ajouter(), null);
			jContentPane.add(getLst_bases(), null);
			jContentPane.add(getBtn_valider(), null);
			jContentPane.add(getBtn_annuler(), null);
			jContentPane.add(jLabel2, null);
			jContentPane.add(jLabel3, null);
			jContentPane.add(getEdt_degres(), null);
			
			//chargement de la liste des bases deja enregistr�es
			model = (DefaultListModel) lst_bases.getModel();
			bases = (Vector<Base>)Menu.projet.getBases().clone();
			for(int i=0;i<Menu.projet.getBases().size();i++)
				model.addElement(Menu.projet.getBases().get(i).toString());
		}
		return jContentPane;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
