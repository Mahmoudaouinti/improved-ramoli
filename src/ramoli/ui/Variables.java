package ramoli.ui;

import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Rectangle;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import java.awt.Dimension;
import java.awt.Toolkit;

public class Variables extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JLabel jLabel = null;
	private JTextField edt_variable = null;
	private JButton btn_ajouter = null;
	private JList lst_variables = null;
	private JButton btn_valider = null;
	private JButton btn_annuler = null;
	private DefaultListModel model = null;
	private JLabel jLabel1 = null;
	/**
	 * This is the default constructor
	 */
	public Variables() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(493, 270);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("D:/These/RAMOLI/RAMOLI3/src/icon.png"));
		this.setContentPane(getJContentPane());
		this.setTitle("Variables linguistiques");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jLabel1 = new JLabel();
			jLabel1.setBounds(new Rectangle(52, 79, 199, 16));
			jLabel1.setText("Liste des variables linguistiques :");
			jLabel = new JLabel();
			jLabel.setBounds(new Rectangle(52, 38, 132, 16));
			jLabel.setText("Variable linguistique :");
			jContentPane = new JPanel();
			jContentPane.setLayout(null);
			jContentPane.add(jLabel, null);
			jContentPane.add(getEdt_variable(), null);
			jContentPane.add(getBtn_ajouter(), null);
			jContentPane.add(getLst_variables(), null);
			jContentPane.add(getBtn_valider(), null);
			jContentPane.add(getBtn_annuler(), null);
			jContentPane.add(jLabel1, null);
			
			//chargement de la liste des variables deja enregistr�es
			model = (DefaultListModel) lst_variables.getModel();
			for(int i=0;i<Menu.projet.getVariables().size();i++)
				model.addElement(Menu.projet.getVariables().elementAt(i));
		}
		return jContentPane;
	}

	/**
	 * This method initializes edt_variable	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getEdt_variable() {
		if (edt_variable == null) {
			edt_variable = new JTextField();
			edt_variable.setBounds(new Rectangle(215, 39, 129, 20));
		}
		return edt_variable;
	}

	/**
	 * This method initializes btn_ajouter	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_ajouter() {
		if (btn_ajouter == null) {
			btn_ajouter = new JButton();
			btn_ajouter.setBounds(new Rectangle(369, 40, 84, 29));
			btn_ajouter.setText("Ajouter");
			btn_ajouter.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if(edt_variable.getText().equals("")){
						JOptionPane.showMessageDialog(null,"Veuillez introduire le nom de la base.");
						edt_variable.requestFocus();
					}
					else if(!model.contains(edt_variable.getText())) {
						model.addElement(edt_variable.getText());
						edt_variable.setText("");
						edt_variable.requestFocus();
					}
					else JOptionPane.showMessageDialog(null, "Cette variable a �t� d�j� ajout�e.");
				}
			});
		}
		return btn_ajouter;
	}

	/**
	 * This method initializes lst_variables	
	 * 	
	 * @return javax.swing.JList	
	 */
	private JList getLst_variables() {
		if (lst_variables == null) {
			lst_variables = new JList(new DefaultListModel());
			lst_variables.setBounds(new Rectangle(54, 105, 188, 115));
		}
		return lst_variables;
	}

	/**
	 * This method initializes btn_valider	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_valider() {
		if (btn_valider == null) {
			btn_valider = new JButton();
			btn_valider.setBounds(new Rectangle(369, 99, 85, 27));
			btn_valider.setText("Valider");
			btn_valider.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Menu.projet.getVariables().clear();
					for(int i=0;i<model.size();i++)
						Menu.projet.addVariable((String)model.elementAt(i));
					dispose(); 
				}
			});
		}
		return btn_valider;
	}

	/**
	 * This method initializes btn_annuler	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getBtn_annuler() {
		if (btn_annuler == null) {
			btn_annuler = new JButton();
			btn_annuler.setBounds(new Rectangle(370, 158, 85, 27));
			btn_annuler.setText("Annuler");
			btn_annuler.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					dispose(); 
				}
			});
		}
		return btn_annuler;
	}

}  //  @jve:decl-index=0:visual-constraint="25,15"
