package ramoli.ui;

import ramoli.connaissances.*;
import ramoli.moteur.*;
import ramoli.util.*;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import java.io.File;

import javax.swing.SwingUtilities;
import java.awt.BorderLayout;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import java.awt.Dimension;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.util.Date;
import java.awt.Toolkit;

public class Menu extends JFrame {
	
	public static Projet projet;

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JMenuBar jJMenuBar = null;
	private JMenu jMenu = null;
	private JMenu jMenu1 = null;
	private JMenu jMenu2 = null;
	private JMenuItem jMenuItem = null;
	private JMenuItem jMenuItem1 = null;
	private JMenuItem jMenuItem2 = null;
	private JMenuItem jMenuItem3 = null;
	private JMenuItem jMenuItem4 = null;
	private JMenuItem jMenuItem5 = null;
	private JMenuItem jMenuItem6 = null;
	private JMenuItem jMenuItem7 = null;
	private JMenuItem jMenuItem8 = null;
	private JMenuItem jMenuItem9 = null;
	private JMenuItem jMenuItem10 = null;
	
	private JMenu jMenu3 = null;

	private JMenuItem jMenuItem12 = null;

	private JMenuItem jMenuItem13 = null;

	/**
	 * This method initializes jJMenuBar	
	 * 	
	 * @return javax.swing.JMenuBar	
	 */
	private JMenuBar getJJMenuBar() {
		if (jJMenuBar == null) {
			jJMenuBar = new JMenuBar();
			jJMenuBar.setPreferredSize(new Dimension(0, 35));
			jJMenuBar.add(getJMenu());
			jJMenuBar.add(getJMenu1());
			jJMenuBar.add(getJMenu2());
			jJMenuBar.add(getJMenu3());
		}
		return jJMenuBar;
	}

	/**
	 * This method initializes jMenu	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu() {
		if (jMenu == null) {
			jMenu = new JMenu();
			jMenu.setText("Projet");
			jMenu.add(getJMenuItem());
			jMenu.add(getJMenuItem1());
			jMenu.add(getJMenuItem2());
			jMenu.add(getJMenuItem10());
			jMenu.add(getJMenuItem12());
			jMenu.add(getJMenuItem13());
		}
		return jMenu;
	}

	/**
	 * This method initializes jMenu1	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu1() {
		if (jMenu1 == null) {
			jMenu1 = new JMenu();
			jMenu1.setText("Connaissances");
			jMenu1.add(getJMenuItem7());
			jMenu1.add(getJMenuItem5());
			jMenu1.add(getJMenuItem6());
			jMenu1.add(getJMenuItem8());
			jMenu1.add(getJMenuItem9());
		}
		return jMenu1;
	}

	/**
	 * This method initializes jMenu2	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu2() {
		if (jMenu2 == null) {
			jMenu2 = new JMenu();
			jMenu2.setText("Inference");
			jMenu2.add(getJMenuItem3());
			jMenu2.add(getJMenuItem4());
		}
		return jMenu2;
	}

	/**
	 * This method initializes jMenuItem	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem() {
		if (jMenuItem == null) {
			jMenuItem = new JMenuItem();
			jMenuItem.setText("Nouveau");
			jMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if(projet!=null){
						String message = "Enregistrer le projet avant de le fermer?";
						String titre = "Fermer le projet";
						int rep = JOptionPane.showConfirmDialog(null, message, titre, JOptionPane.YES_NO_CANCEL_OPTION);
						if (rep == JOptionPane.YES_OPTION)
							GestionProjet.saveProjet(projet);
						if(rep != JOptionPane.CANCEL_OPTION){
							fermerProjet();
							JFileChooser chooser = new JFileChooser();
							chooser.setDialogTitle("Cr�er un projet");
							FileFilter filter = new RamoliFileFilter();				
							chooser.removeChoosableFileFilter(chooser.getAcceptAllFileFilter());
							chooser.setFileFilter(filter);

							int returnVal = chooser.showSaveDialog(jContentPane);
							if(returnVal == JFileChooser.APPROVE_OPTION) {
								//creation d'un nouveau projet
								projet = new Projet(remExtension(chooser.getSelectedFile().getName()));
								projet.setChemin(remExtension(chooser.getSelectedFile().getPath()));
								projet.setDateCreation(new Date());
								GestionProjet.saveProjet(projet);
								setTitle("RAMOLI - "+projet.getChemin()+".ramoli");
								setEnabledMenus(true);
							}
					    }
					}
					else{
						JFileChooser chooser = new JFileChooser();
						chooser.setDialogTitle("Cr�er un projet");
						FileFilter filter = new RamoliFileFilter();				
						chooser.removeChoosableFileFilter(chooser.getAcceptAllFileFilter());
						chooser.setFileFilter(filter);

						int returnVal = chooser.showSaveDialog(jContentPane);
						if(returnVal == JFileChooser.APPROVE_OPTION) {
							//creation d'un nouveau projet
							projet = new Projet(remExtension(chooser.getSelectedFile().getName()));
							projet.setChemin(remExtension(chooser.getSelectedFile().getPath()));
							projet.setDateCreation(new Date());
							GestionProjet.saveProjet(projet);
							setTitle("RAMOLI - "+projet.getChemin()+".ramoli");
							setEnabledMenus(true);
						}
					}
				}
			});
		}
		return jMenuItem;
	}

	/**
	 * This method initializes jMenuItem1	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem1() {
		if (jMenuItem1 == null) {
			jMenuItem1 = new JMenuItem();
			jMenuItem1.setText("Ouvrir");
			jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if(projet!=null){
						String message = "Enregistrer le projet avant de le fermer?";
						String titre = "Fermer le projet";
						int rep = JOptionPane.showConfirmDialog(null, message, titre, JOptionPane.YES_NO_CANCEL_OPTION);
						if (rep == JOptionPane.YES_OPTION)
							GestionProjet.saveProjet(projet);
						if(rep != JOptionPane.CANCEL_OPTION){
							fermerProjet();
							
							JFileChooser chooser = new JFileChooser();
							chooser.setDialogTitle("Ouvrir un Projet");
							FileFilter filter = new RamoliFileFilter();				
							chooser.removeChoosableFileFilter(chooser.getAcceptAllFileFilter());
							chooser.setFileFilter(filter);

							int returnVal = chooser.showOpenDialog(jContentPane);
							if(returnVal == JFileChooser.APPROVE_OPTION) {
								//ouvrir le projet
								String chemin = remExtension(chooser.getSelectedFile().getPath());
								chemin+=".ramoli";
								projet = GestionProjet.openProjet(chemin);
								setTitle("RAMOLI - "+projet.getChemin()+".ramoli");
								setEnabledMenus(true);
							}
					    }
					}
					else{	
						JFileChooser chooser = new JFileChooser();
						chooser.setDialogTitle("Ouvrir un Projet");
						FileFilter filter = new RamoliFileFilter();				
						chooser.removeChoosableFileFilter(chooser.getAcceptAllFileFilter());
						chooser.setFileFilter(filter);
	
						int returnVal = chooser.showOpenDialog(jContentPane);
						if(returnVal == JFileChooser.APPROVE_OPTION) {
							//ouvrir le projet
							String chemin = remExtension(chooser.getSelectedFile().getPath());
							chemin+=".ramoli";
							projet = GestionProjet.openProjet(chemin);
							setTitle("RAMOLI - "+projet.getChemin()+".ramoli");
							setEnabledMenus(true);
						}
					}
				}
			});
		}
		return jMenuItem1;
	}

	/**
	 * This method initializes jMenuItem2	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem2() {
		if (jMenuItem2 == null) {
			jMenuItem2 = new JMenuItem();
			jMenuItem2.setText("Enregister");
			jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					GestionProjet.saveProjet(projet);
				}
			});
		}
		return jMenuItem2;
	}

	/**
	 * This method initializes jMenuItem3	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem3() {
		if (jMenuItem3 == null) {
			jMenuItem3 = new JMenuItem();
			jMenuItem3.setText("Raisonnement exact");
			jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					MoteurExact me = new MoteurExact(projet);
					String res = me.chainageAvant();
					JOptionPane.showMessageDialog(null, res);
				}
			});
		}
		return jMenuItem3;
	}

	/**
	 * This method initializes jMenuItem4	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem4() {
		if (jMenuItem4 == null) {
			jMenuItem4 = new JMenuItem();
			jMenuItem4.setText("Raisonnement approximatif");
			jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					MoteurApproximatif me = new MoteurApproximatif(projet);
					String res = me.chainageAvant();
					JOptionPane.showMessageDialog(null,res);
				}
			});
		}
		return jMenuItem4;
	}

	/**
	 * This method initializes jMenuItem5	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem5() {
		if (jMenuItem5 == null) {
			jMenuItem5 = new JMenuItem();
			jMenuItem5.setText("Variables linguistiques");
			jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Variables f = new Variables();
					f.show();
				}
			});
		}
		return jMenuItem5;
	}

	/**
	 * This method initializes jMenuItem6	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem6() {
		if (jMenuItem6 == null) {
			jMenuItem6 = new JMenuItem();
			jMenuItem6.setText("Predicats");
			jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Predicats f = new Predicats();
					f.show();
				}
			});
		}
		return jMenuItem6;
	}

	/**
	 * This method initializes jMenuItem7	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem7() {
		if (jMenuItem7 == null) {
			jMenuItem7 = new JMenuItem();
			jMenuItem7.setText("Bases");
			jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Bases f = new Bases();
					f.show();
				}
			});
		}
		return jMenuItem7;
	}

	/**
	 * This method initializes jMenuItem8	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem8() {
		if (jMenuItem8 == null) {
			jMenuItem8 = new JMenuItem();
			jMenuItem8.setText("Faits");
			jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Faits f = new Faits();
					f.show();
				}
			});
		}
		return jMenuItem8;
	}

	/**
	 * This method initializes jMenuItem9	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem9() {
		if (jMenuItem9 == null) {
			jMenuItem9 = new JMenuItem();
			jMenuItem9.setText("Regles");
			jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Regles f = new Regles();
					f.show();
				}
			});
		}
		return jMenuItem9;
	}

	/**
	 * This method initializes jMenuItem10	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem10() {
		if (jMenuItem10 == null) {
			jMenuItem10 = new JMenuItem();
			jMenuItem10.setText("Fermer");
			jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					String message = "Enregistrer le projet avant de le fermer?";
					String titre = "Fermer le projet";
					int rep = JOptionPane.showConfirmDialog(null, message, titre, JOptionPane.YES_NO_CANCEL_OPTION);
					if (rep == JOptionPane.YES_OPTION)
					{
						GestionProjet.saveProjet(projet);
						fermerProjet();
				    }
					else if(rep == JOptionPane.NO_OPTION){ 
						fermerProjet();
					}
					
				}
			});
		}
		return jMenuItem10;
	}


	/**
	 * This method initializes jMenu3	
	 * 	
	 * @return javax.swing.JMenu	
	 */
	private JMenu getJMenu3() {
		if (jMenu3 == null) {
			jMenu3 = new JMenu();
			jMenu3.setText("?");
		}
		return jMenu3;
	}

	/**
	 * This method initializes jMenuItem12	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem12() {
		if (jMenuItem12 == null) {
			jMenuItem12 = new JMenuItem();
			jMenuItem12.setText("----------------");
		}
		return jMenuItem12;
	}

	/**
	 * This method initializes jMenuItem13	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJMenuItem13() {
		if (jMenuItem13 == null) {
			jMenuItem13 = new JMenuItem();
			jMenuItem13.setText("Quitter");
			jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if(projet!=null) {
						String message = "Enregistrer le projet avant de quitter?";
						String titre = "Quitter RAMOLI";
						int rep = JOptionPane.showConfirmDialog(null, message, titre, JOptionPane.YES_NO_CANCEL_OPTION);
						if (rep == JOptionPane.YES_OPTION)
					    {
							GestionProjet.saveProjet(projet);
							System.exit(0);
					    }
						else if(rep == JOptionPane.NO_OPTION) 
							System.exit(0);
					}
					else System.exit(0);
				}
			});
		}
		return jMenuItem13;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Menu thisClass = new Menu();
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setVisible(true);
			}
		});
	}

	/**
	 * This is the default constructor
	 */
	public Menu() {
		super();
		initialize();
		setEnabledMenus(false);
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(514, 260);
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("D:/These/RAMOLI/RAMOLI3/src/icon.png"));
		this.setJMenuBar(getJJMenuBar());
		this.setContentPane(getJContentPane());
		this.setTitle("RAMOLI");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
		}
		return jContentPane;
	}
	
	public String remExtension(String s){
		if(s.length()<8)return s;
		else if(s.substring(s.length()-7).equalsIgnoreCase(".Ramoli"))
			return s.substring(0, s.length()-7);
		else return s;
	}
	
	public void setEnabledMenus(boolean b){
		jMenuItem2.setEnabled(b);
		jMenuItem3.setEnabled(b);
		jMenuItem4.setEnabled(b);
		jMenuItem5.setEnabled(b);
		jMenuItem6.setEnabled(b);
		jMenuItem7.setEnabled(b);
		jMenuItem8.setEnabled(b);
		jMenuItem9.setEnabled(b);
		jMenuItem10.setEnabled(b);
	}
	
	public void fermerProjet(){
		projet = null;
		setEnabledMenus(false);
		setTitle("RAMOLI");
	}

}  //  @jve:decl-index=0:visual-constraint="35,17"
