package ramoli.connaissances;

import java.io.Serializable;

import ramoli.ui.Menu;

public class Proposition implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = 5089432756567073910L;
	protected String variable;
	protected MultiEnsemble me;
	public Predicat p;
	
	
	
public Proposition(){}
	
	public Proposition(String v ,MultiEnsemble m){
		variable = v;
		me = m;
		
	}
	
	public Proposition(String var,int degre,String pre){
		Predicat p = Menu.projet.getPredicat(pre);
		MultiEnsemble m = new MultiEnsemble(p,degre);
		me = m;
		variable = var;
	}
	
	public Proposition(String var ,int degre, Predicat pre){
		MultiEnsemble m = new MultiEnsemble(pre,degre);
		me = m;
		variable = var;
	}
	
	
	public void setVariable(String v){
		variable = v;
	}
	
	public void setMultiEnsemble(MultiEnsemble d){
		me = d;
	} 
	
	
	
	public String getVariable(){
		return  variable;
	}
	
	public MultiEnsemble getMultiEnsemble(){
		return me;
	}
	
	
	
	
	public String toString(){
		if(me.getTermeDegre()==null)
			return variable + " est v" + me.getDegre() + " " + me.getPredicat().getNom();
		else
			return variable + " est " + me.getTermeDegre() + " " + me.getPredicat().getNom(); 
			
		
		
	}
	
	public Proposition clone(){
		return new Proposition(variable, new MultiEnsemble(me.getPredicat(),me.getDegre()));
		
	}

	public void add(Proposition p) {
		// TODO Auto-generated method stub
		
	}
}
