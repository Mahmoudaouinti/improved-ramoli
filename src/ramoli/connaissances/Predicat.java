package ramoli.connaissances;

import java.io.Serializable;

import ramoli.ui.Menu;

public class Predicat implements Serializable{
	
	private Predicat predicat;
	private String nom;
	private Base base;
	
	public Predicat(String n, Base b){
		nom = n;
		
		base = b;
	}
	
	public Predicat(String n,String b){
		nom = n;
		
		int i = 0;
		while(i<Menu.projet.getBases().size() && !Menu.projet.getBases().elementAt(i).getNom().equals(b))
			i++;
		base = Menu.projet.getBases().elementAt(i);
	}
	
	public void setNom(String n){
		nom = n;
		
	}
	
	public String getNom(){
		return nom;
	}
	
	public void setBase(Base b){
		base = b;
	}
	
	public Base getBase(){
		return base;
	}
	
	public String toString(){
		return "Pr�dicat : "+nom+"   "
				+ " Base : "+base.getNom();
	}
	public void setPredicat(Predicat p){
		predicat = p;
	}
	
	public Predicat getPredicat(){
		return  predicat;
	}



}
