package ramoli.connaissances;

import java.io.Serializable;

public class MultiEnsemble implements Serializable{
	
	private Predicat predicat;
	private int degre;
		
	public MultiEnsemble(){}
	
	public MultiEnsemble(Predicat p,int d){
		predicat = p;
		degre = d;
		
	}
	
	
	public void setPredicat(Predicat p){
		predicat = p;
	}
	
	public void setDegre(int d){
		degre = d;
	}
	
	public Predicat getPredicat(){
		return  predicat;
	}
	
	public int getDegre(){
		return degre;
	}
	
	public String getTermeDegre(){
		if(!predicat.getBase().avecEchelle()) return null;
		return predicat.getBase().getEchelle().elementAt(degre);
	} 

}
