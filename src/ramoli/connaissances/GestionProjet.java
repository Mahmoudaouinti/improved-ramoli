package ramoli.connaissances;
import java.util.Vector;
import java.io.*;

import javax.swing.JOptionPane;



public class GestionProjet {
	
	public static void initProjet(Projet p){
	/*	Vector<String> echelle9 = new Vector<String>();
		echelle9.add("pas-du-tout");
		echelle9.add("tr�s peu");
		echelle9.add("peu");
		echelle9.add("plus-ou-moins");
		echelle9.add("moyennement");
		echelle9.add("plutot");
		echelle9.add("assez");
		echelle9.add("tr�s");
		echelle9.add("tout-a-fait");
		Base L9 = new Base("L9",9,echelle9); */
		
		Vector<String> echelle = new Vector<String>();
		echelle.add("pas-du-tout");
		echelle.add("tr�s peu");
		echelle.add("peu");
		echelle.add("moyennement");
		echelle.add("assez");
		echelle.add("tr�s");
		echelle.add("tout-a-fait");
		Base L7 = new Base("L7",7,echelle);
		
	/*	Vector<String> echelle1 = new Vector<String>();
		echelle1.add("pas-du-tout");
		echelle1.add("peu");
		echelle1.add("moyennement");
		echelle1.add("assez");
		echelle1.add("tout-a-fait");
		Base L5 = new Base("L5",5,echelle1); */
		
/*		Vector<String> echelle2 = new Vector<String>();
		echelle2.add("pas-du-tout");
		echelle2.add("moyennement");
		echelle2.add("tout-a-fait");
		Base L3 = new Base("L3",3,echelle2); */
		
/*		Vector<String> echelle3 = new Vector<String>();
		echelle3.add("pas-du-tout");
		echelle3.add("tout-a-fait");
		Base L2 = new Base("L2",2,echelle3); */
		
		Vector<Base> bases = new Vector<Base>();
	//	bases.add(L2);
		//bases.add(L3);
	//	bases.add(L5);
		bases.add(L7);
	//	bases.add(L9);
		
		p.setBases(bases);
	}
		
	public static void saveProjet(Projet p){
		ObjectOutputStream  f;
		try{
	    	f = new ObjectOutputStream (new FileOutputStream(p.getChemin()+".ramoli"));
	    	f.writeObject(p);
	    	f.flush();
	    	f.close();
	    }
	    catch(IOException e)
	    {e.printStackTrace();
	    	JOptionPane.showMessageDialog(null,"Erreur d'enregistrement du projet");}
	    
		
	}
	
	public static Projet openProjet(String chemin){
		ObjectInputStream f;
		Projet p = new Projet();
		try{
	    	f = new ObjectInputStream(new BufferedInputStream
				  (new FileInputStream(chemin)));
	    	p = (Projet) f.readObject();
	    	f.close();
	    	Fait.idMax = idMaxFait(p.getBF());
	    	Regle.idMax = idMaxRegle(p.getBR());
	    }
	    catch(IOException e)
	    {
	    	e.printStackTrace();
	    	JOptionPane.showMessageDialog(null,"Erreur d'ouverture du projet");
	    }
	    catch(ClassNotFoundException e)
	    {e.printStackTrace();}
	return p;
	}
	
	private static int idMaxFait(Vector<Fait> bf){
		int res = 0;
		for(int i=0;i<bf.size();i++){
			if(bf.get(i).getId()>res)
				res = bf.get(i).getId();
		}
		return res;
	}
	
	private static int idMaxRegle(Vector<Regle> br){
		int res = 0;
		for(int i=0;i<br.size();i++){
			if(br.get(i).getId()>res)
				res = br.get(i).getId();
		}
		return res;
	}
}
