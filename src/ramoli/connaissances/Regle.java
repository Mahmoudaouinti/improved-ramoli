	package ramoli.connaissances;

	import java.io.Serializable;
	import java.util.Vector;

	public class Regle implements Serializable {
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;
		static int idMax = 0;
		private int id;
		private char jonction;// 'c' pour conjonction et 'd' pour disjonction
		private Proposition conclusion;

		/*
		 * (non-javadoc)
		 */
			private Vector<Proposition> premisse;
		

		/**
		 * Getter of the property <tt>premisse</tt>
		 * 
		 * @return Returns the premisse.
		 * 
		 */

		public Vector<Proposition> getPremisse() {
			
			return premisse;
		}

		/**
		 * Setter of the property <tt>premisse</tt>
		 * 
		 * @param premisse
		 *            The premisse to set.
		 * 
		 */
		public void setPremisse(Vector<Proposition> premisse) {
			
			this.premisse = premisse;
		}

		public Regle() {
			idMax++;
			id = idMax;
		}

			public Regle(Vector<Proposition> p, Proposition c) {
			
			idMax++;
			id = idMax;
			premisse = p;
			conclusion = c;
			jonction = 'c';
		}

		public Regle(Proposition p, Proposition c) {
			idMax++;
			id = idMax;
			Vector<Proposition> v = new Vector<Proposition>();
			
			v.add(p);
			premisse = v;
			conclusion = c;
			jonction = 'c';
		}

		public void setConclusion(Proposition c) {
			conclusion = c;
		}

		public void setId(int i) {
			id = i;
			if (id > idMax)
				idMax = id;
		}

		public int getId() {
			return id;
		}

		public Proposition getConclusion() {
			return conclusion;
		}

		public void setJonction(char j) {
			jonction = j;
		}

		public char getJonction() {
			return jonction;
		}

		public String toString() {
			String res = "R" + id + " : Si ";
			for (int i = 0; i < premisse.size(); i++) {
				res += premisse.get(i);
				if (i != premisse.size() - 1)
					res += " et ";
			}  
			
			return res += " alors " + conclusion;
		}
	}

//public Vector<Proposition> getPremisse(){
//		return premisse;
//	}
//public void setPremisse(Vector<Proposition> p){
//		premisse = p;
//	}