package ramoli.connaissances;

import java.io.Serializable;

public class Fait extends Proposition implements Serializable{
	static int idMax=0;
	private int id = 0;
	
	public Fait(String var,MultiEnsemble me)
	{
		super(var,me);
		idMax++;
		id = idMax;
	}
	
	public Fait(Proposition p){
		super(p.getVariable(),p.getMultiEnsemble());
		idMax++;
		id = idMax;
	}
	
	public Fait(String var,String pre){
		super();
		idMax++;
		id = idMax;
	}
	
	public int getId(){
		return id;
	}
	
	public void setId(int i){
		id = i;
		if(id>idMax)idMax = id;
	}
	
	public String toString(){
		return "F" + id + " : " + super.toString();
	}

}