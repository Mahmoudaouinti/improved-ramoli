package ramoli.connaissances;

import java.io.Serializable;
import java.util.Vector;

public class Base implements Serializable{
	
	private String nom;
	private int taille;
	private Vector<String> echelle = new Vector<String>();

	public Base()
	{}
	
	public Base(int t){
		taille = t;
	}
	
	public Base(String n,int t){
		nom = n;
		taille = t;
	}
	
	public Base(String n,int t,Vector<String> e){
		nom = n;
		taille = t;
		echelle = e;
	}
	
	public void setTaille(int t){
		taille = t;
	}
	
	public void setEchelle(Vector<String> e){
		if(e.size()>taille) System.out.println("erreur d'affectation de base");
		else echelle = e;
	}
	
	public int getTaille(){
		return taille;
	}
	
	public String getNom(){
		return nom;
	}
	
	public Vector<String> getEchelle(){
		return echelle;
	}
	
	public boolean avecEchelle(){
		return echelle.size()==taille;
	}
	
	public String getTermeDegre(int d){
		if(!avecEchelle() || d<0 || d>taille) return null;
		return echelle.elementAt(d);
	}
	
	public String toString(){
		String res = "Nom : "+nom+", taille :"+taille;
		if(avecEchelle()){
			res+=", echelle = { ";
			for(int i=0;i<echelle.size();i++){
				res+=echelle.get(i);
				if(!(i==echelle.size()-1)) res+="; ";
			}
			res+=" }";
		}
		return res;
	}
}
