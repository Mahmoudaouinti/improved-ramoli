package ramoli.connaissances;

import java.io.Serializable;
import java.util.Date;
import java.util.Vector;


public class Projet implements Serializable{
	private String nom;
	private String chemin;
	private String auteur;
	private Date dateCreation;
	private Vector<String> variables = new Vector<String>();
	private Vector<Predicat> predicats = new Vector<Predicat>();
	private Vector<Base> bases = new Vector<Base>();
	private Vector<Regle> BR = new Vector<Regle>();
	private Vector<Fait> BF = new Vector<Fait>();
	private String tnorm="TL";
	private String tconorm="SL";
	
	public Projet(){
		Fait.idMax = 0;
		Regle.idMax = 0;
		GestionProjet.initProjet(this);
	}
	
	public Projet(String n){
		Fait.idMax = 0;
		Regle.idMax = 0;
		nom = n;
		GestionProjet.initProjet(this);
	}
	
	public void setNom(String n){auteur = n;}
	
	public void setChemin(String c){chemin = c;}
		
	public void setAuteur(String a){auteur = a;}
	
	public void setDateCreation(Date d){dateCreation = d;}
	
	public void setBR(Vector<Regle> b){BR = b;}
	
	public void setBF(Vector<Fait> b){BF = b;}
	
	public String getNom(){return nom;}
	
	public String getChemin(){return chemin;}
	
	public String getAuteur(){return auteur;}
	
	public Date getDateCreation(){return dateCreation;}
	
	public Vector<Regle> getBR() {return BR;}

	public Vector<Fait> getBF() {return BF;}
	
	/**
	 * permet de rechercher un fait dans la base des faits du projet
	 * selon son id
	 * @param id
	 * @return
	 */
	
	public Fait getFait(int id){
		int i = 0;
		while (i<BF.size() && (BF.elementAt(i)).getId() != id)
			i++;
		if(i==BF.size()) return  BF.elementAt(i);
		else return null;
	}
	
	/**
	 * permet d'ajouter un fait f � la base des faits du projet
	 * @param f
	 * @return 
	 */
	
	public String addFait(Proposition p){
		String res = "";
		//verifier d'abord si le fait existe deja
		//s'il existe on prend celui qui a le plus grand degre
		int i = 0;
		boolean trouv = false;
		while(i<BF.size() && !trouv){
			if(BF.get(i).getVariable().equals(p.getVariable()) &&
					BF.get(i).getMultiEnsemble().getPredicat().getNom().equals(p.getMultiEnsemble().getPredicat().getNom())){
				if(BF.get(i).getMultiEnsemble().getDegre() < p.getMultiEnsemble().getDegre()){
					res = "Fait mis � jour " + BF.get(i) + " --> ";
					BF.get(i).getMultiEnsemble().setDegre(p.getMultiEnsemble().getDegre());
					res += BF.get(i);
				}
				else res = "Fait d�j� pr�sent " + BF.get(i);
				trouv = true;
			}
			i++;
		}
		if(!trouv){
			Fait f = new Fait(p);
			BF.addElement(f);
			res = "Fait ajout� "+f;
		}
		return res;
		
		
		
		
	}
	
	/**
	 * supprimer un fait de la base des faits selon son id
	 * @param id
	 */
	public void removeFait(int id){
		int i = 0;
		while (i<BF.size() && (BF.elementAt(i)).getId() != id)
			i++;
		if(i!=BF.size()) BF.removeElementAt(i);
	}
	
	public Regle getRegle(int id){
		int i = 0;
		while (i<BR.size() && (BR.elementAt(i)).getId() != id)
			i++;
		if(i==BR.size()) return  BR.elementAt(i);
		else return null;
	}
	
	public void addRegle(Regle f){BR.addElement(f);}
	
	public void removeRegle(int id){
		int i = 0;
		while (i<BR.size() && (BR.elementAt(i)).getId() != id)
			i++;
		if(i!=BR.size()) BR.removeElementAt(i);
	}
	
	public Base getBase(String n){
		int i = 0;
		while (i<bases.size() && !(bases.elementAt(i)).getNom().equals(n))
			i++;
		if(i==bases.size()) return null;
		else return bases.elementAt(i);
	}
	
	public void addBase(Base b){
		bases.addElement(b);
	}
	
	public void removeBase(String n){
		int i = 0;
		while (i<bases.size() && !(bases.elementAt(i)).getNom().equals(n))
			i++;
		if(i!=bases.size()) bases.removeElementAt(i);
	}
	
	public Vector<Base> getBases(){
		return bases;
	}
	
	public void setBases(Vector<Base> b){
		bases = b;
	}
	
	public Vector<Predicat> getPredicats(){
		return predicats;
	}
	
	public void setPredicats(Vector<Predicat> p){
		predicats = p;
	}
	
	public void addPredicat(Predicat p){
		predicats.addElement(p);
	}
	
	public void removePredicat(String n){
		int i = 0;
		while (i<predicats.size() && !(predicats.elementAt(i)).equals(n))
			i++;
		if(i!=predicats.size()) predicats.removeElementAt(i);
	}
	
	public Predicat getPredicat(String s){
		int i = 0;
		while(i<predicats.size() && !predicats.elementAt(i).getNom().equals(s))
			i++;
		if(i==predicats.size()) return null;
		return predicats.elementAt(i);
	}
	
	public Vector<String> getVariables(){
		return variables;
	}
	
	public void setVariables(Vector<String> v){
		variables = v;
	}
	
	public void addVariable(String v){
		variables.addElement(v);
	}
	
	public void removeVariable(String v){
		int i = 0;
		while (i<variables.size() && !(variables.elementAt(i)).equals(v))
			i++;
		if(i!=variables.size()) variables.removeElementAt(i);
	}
	
	public Vector<String> getDegres(String nomPredicat){
		if(nomPredicat==null || nomPredicat.equals("")) return new Vector<String>();
		int i = 0;
		while(i<predicats.size() && !predicats.elementAt(i).getNom().equals(nomPredicat))
			i++;
		if(predicats.elementAt(i).getBase().avecEchelle()) 
			return predicats.elementAt(i).getBase().getEchelle();
		Vector<String> degres = new Vector<String>();
		for(int j=0;j<predicats.elementAt(i).getBase().getTaille();j++){
			degres.add("v"+j);
		}
		return degres; 
	} 
	
	public int getDegreFait(String var, String pre){
		int res = 0;
		int i = 0;
		boolean trouv = false;
		while(i< BF.size() && !trouv){
			if(BF.get(i).getVariable().equals(var) && 
					BF.get(i).getMultiEnsemble().getPredicat().getNom().equals(pre)){
				trouv = true;
				res = BF.get(i).getMultiEnsemble().getDegre();
			}
			i++;
		}
		return res;
	}
	
	public void setTnorm(String t){
		tnorm = t;
	}
	
	public String getTnorm(){return tnorm;}
	
	public void setTconorm(String t){
		tconorm = t;
	}
	
	public String getTconorm(){return tnorm;}
	
	public String toString(){
		return chemin+dateCreation;
	}
}
