package ramoli.modificateurs;

import ramoli.connaissances.MultiEnsemble;
import ramoli.connaissances.Base;
import ramoli.util.Connecteurs;

/**
 * <p>Title: SuCRAGe</p>
 * <p>Description: Supervised Classification by Rules Automatic Generation </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: LI3</p>
 * @author Saoussen Bel Hadj Kacem
 * @version 4.0
 */


public class CW extends MSG {

  public CW()
  {this.nom = "CW";}

  public CW(int rho)
  {
    this.rho = rho;
    this.nom = "CW";
  }

  public MultiEnsemble trans(MultiEnsemble me) {
    int degre = me.getDegre();
    int resDegre = Math.max(0,degre-rho);
    return new MultiEnsemble(me.getPredicat(),resDegre);
  }
  
  public MSG mnorm(MSG m, String tnorm,int M){
	  if(m.getNom().equals("CC")||m.getNom().equals("CR")) return new CW(rho);
	  else if(m.getNom().equals("CW")){
		  int resRho = Connecteurs.dualTnorm(rho, m.getRho(), tnorm,M);
		  if(resRho==0) return new CC();
		  else return new CW(resRho);
	  }
	  else return null;
  }
  
  public MSG mconorm(MSG m, String tconorm,int M){
	  if(m.getNom().equals("CC")) return new CC();
	  else if(m.getNom().equals("CW")){
		  int resRho = Connecteurs.dualTconorm(rho, m.getRho(), tconorm,M);
		  if(resRho==0) return new CC();
		  else return new CW(resRho);
	  }
	  else if(m.getNom().equals("CR"))return new CR(m.getRho());
	  else return null;
  }

  public String toString()
  {return "CW(" + rho + ")";}

}
