package ramoli.modificateurs;

import ramoli.connaissances.MultiEnsemble;

/**
 * <p>Title: SuCRAGe</p>
 * <p>Description: Supervised Classification by Rules Automatic Generation </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: LI3</p>
 * @author Saoussen Bel Hadj Kacem
 * @version 4.0
 */


public class CC extends MSG {

  public CC()
  {
    this.rho = 0;
    nom = "CC";
  }

  public MultiEnsemble trans(MultiEnsemble me) {
    return me;
  }
  
  public MSG mnorm(MSG m, String tnorm,int M){
	  if(m.getNom().equals("CC")|| m.getNom().equals("CR")) return new CC();
	  else if(m.getNom().equals("CW")) return new CW(m.getRho());
	  else return null;
  }
  
  public MSG mconorm(MSG m, String tconorm,int M){
	  if(m.getNom().equals("CC")|| m.getNom().equals("CW")) return new CC();
	  else if(m.getNom().equals("CR")) return new CR(m.getRho());
	  else return null;
  }

  public String toString()
  {return "CC";}

}
