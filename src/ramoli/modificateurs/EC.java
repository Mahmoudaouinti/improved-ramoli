package ramoli.modificateurs;

import ramoli.connaissances.Base;
import ramoli.connaissances.MultiEnsemble;
import ramoli.connaissances.Predicat;

public class EC extends MSG{
	public EC()
	  {nom = "EC";}

	  public EC(int rho)
	  {
	    this.rho = rho;
	    this.nom = "EC";
	  }

	  public MultiEnsemble trans(MultiEnsemble me) {
	    int degre = me.getDegre();
	    Base base = me.getPredicat().getBase();
	    int resDegre = Math.max((int)Math.floor((double)degre/rho), 1);
	    int resBase = Math.max((int)Math.floor((double)base.getTaille()/rho)+1, 2);
	    return new MultiEnsemble(new Predicat(me.getPredicat().getNom(),new Base(resBase)),resDegre);
	  }
	  
	  public MSG mnorm(MSG m,String tnorm,int M){return null;}
	  public MSG mconorm(MSG m,String tnorm,int M){return null;}

	  public String toString()
	  {return "EC(" + rho + ")";}


}
