package ramoli.modificateurs;


import ramoli.connaissances.*;
import ramoli.util.Connecteurs;
import java.util.Vector;

/**
 * <p>Title: SuCRAGe</p>
 * <p>Description: Supervised Classification by Rules Automatic Generation </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: LI3</p>
 * @author Saoussen Bel Hadj Kacem
 * @version 4.0
 */



public abstract class MSG {

  protected String nom;
  protected int rho;

  public MSG(){}

  public MSG(int rho)
  {this.rho = rho;}

  public abstract MultiEnsemble trans(MultiEnsemble me);
  
  public abstract MSG mnorm(MSG msg, String tnorm,int M);
  
  public abstract MSG mconorm(MSG msg, String tnorm,int M);

  public int getRho()
  {return rho;}

  public void setRho(int rho)
  {this.rho = rho;}

  public String getNom()
  {return nom;}

  public void setNom(String nom)
  {this.nom = nom;}
  
  public static Vector<Proposition> interfacage(Vector<Proposition> p,int M){
	  Vector<Proposition> res = new Vector<Proposition>(p.size());
	  for(int i=0;i<p.size();i++)
		  res.add(interfacage(p.get(i),M));
	  return res;
  }
  
  public static Proposition interfacage(Proposition p, int M){
	  Proposition res = new Proposition();
	  res.setVariable(p.getVariable());
	  DC m = new DC((M-1)/(p.getMultiEnsemble().getPredicat().getBase().getTaille()-1));
	  res.setMultiEnsemble(m.trans(p.getMultiEnsemble()));
	  return res;
  }
  
  public static Proposition desinterfacage(Proposition p, int M){
	  Proposition res = new Proposition();
	  res.setVariable(p.getVariable());
	  EC m = new EC((p.getMultiEnsemble().getPredicat().getBase().getTaille()-1)/(M-1));
	  res.setMultiEnsemble(m.trans(p.getMultiEnsemble()));
	  return res;
  }
  
  
  
  /*
  public static MSG agregation(MSG m1, MSG m2,String agg){
	  MSG res = null;
	  int degre;
	  if(m1.getNom().equals("CC") ||
			  m2.getNom().equals("CC") ||
			  m1.getNom().equals(m2.getNom())){
		  if(m1.getNom().equals("CR")){
			  degre = Connecteurs.agregation(m1.rho,m2.rho,agg);
			  if(degre==0) return new CC();
			  else return new CR(degre);
		  }
		  else if(m1.getNom().equals("CW")){
			  
		  }
		  
	  }
	  return res;
  }*/

  public static MSG mnorm(Vector<MSG> mods,String tnorm,int M){
	  MSG res = mods.get(0);
	  for(int i=1;i<mods.size();i++)
		  res = res.mnorm(mods.get(i),tnorm,M);
	  return res;
  }
  
  public static MSG mconorm(Vector<MSG> mods,String tconorm,int M){
	  MSG res = mods.get(0);
	  for(int i=1;i<mods.size();i++)
		  res = res.mconorm(mods.get(i),tconorm,M);
	  return res;
  }


}
