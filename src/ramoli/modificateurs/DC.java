package ramoli.modificateurs;

import ramoli.connaissances.*;

/**
 * <p>Title: SuCRAGe</p>
 * <p>Description: Supervised Classification by Rules Automatic Generation </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: LI3</p>
 * @author Saoussen Bel Hadj Kacem
 * @version 4.0
 */


public class DC extends MSG {

  public DC()
  {nom = "DC";}

  public DC(int rho)
  {
    this.rho = rho;
    this.nom = "DC";
  }

  public MultiEnsemble trans(MultiEnsemble me) {
    int degre = me.getDegre();
    Base base = me.getPredicat().getBase();
    int resDegre = degre*rho;
    int resBase = base.getTaille()*rho-rho+1;
    return new MultiEnsemble(new Predicat(me.getPredicat().getNom(),new Base(resBase)),resDegre);
  }
  
  public MSG mnorm(MSG m,String tnorm,int M){return null;}
  public MSG mconorm(MSG m,String tnorm,int M){return null;}

  public String toString()
  {return "DC(" + rho + ")";}

}
