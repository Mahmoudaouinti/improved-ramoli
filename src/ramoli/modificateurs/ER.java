package ramoli.modificateurs;

import ramoli.connaissances.*;

/**
 * <p>Title: SuCRAGe</p>
 * <p>Description: Supervised Classification by Rules Automatic Generation </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: LI3</p>
 * @author Saoussen Bel Hadj Kacem
 * @version 4.0
 */


public class ER extends MSG {

  public ER()
  {this.nom = "ER";}

  public ER(int rho)
  {
    this.rho = rho;
    this.nom = "ER";
  }

  public MultiEnsemble trans(MultiEnsemble me) {
    int degre = me.getDegre();
    Base base = me.getPredicat().getBase();
    int resBase = Math.max(degre+1,base.getTaille()-rho);
    return new MultiEnsemble(new Predicat(me.getPredicat().getNom(),new Base(resBase)),degre);
  }
  
  public MSG mnorm(MSG m,String tnorm,int M){return null;}
  public MSG mconorm(MSG m,String tnorm,int M){return null;}

  public String toString()
  {return "ER(" + rho + ")";}


}
