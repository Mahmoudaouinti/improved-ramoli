package ramoli.modificateurs;

import ramoli.connaissances.MultiEnsemble;
import ramoli.connaissances.Base;
import ramoli.util.Connecteurs;

/**
 * <p>Title: SuCRAGe</p>
 * <p>Description: Supervised Classification by Rules Automatic Generation </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: LI3</p>
 * @author Saoussen Bel Hadj Kacem
 * @version 4.0
 */

public class CR extends MSG {

  public CR()
  {this.nom = "CR";}

  public CR(int rho)
  {
    this.rho = rho;
    this.nom = "CR";
  }

  public MultiEnsemble trans(MultiEnsemble me) {
    int degre = me.getDegre();
    Base base = me.getPredicat().getBase();
    int resDegre = Math.min(degre+rho,base.getTaille()-1);
    return new MultiEnsemble(me.getPredicat(),resDegre);
  }
  
  public MSG mnorm(MSG m, String tnorm,int M){
	  if(m.getNom().equals("CC")) return new CC();
	  else if(m.getNom().equals("CR")){
		  int resRho = Connecteurs.tnorm(rho, m.getRho(), tnorm,M);
		  if(resRho==0) return new CC();
		  else return new CR(resRho);
	  }
	  else if(m.getNom().equals("CW"))return new CW(m.getRho());
	  else return null;
  }
  
  public MSG mconorm(MSG m, String tconorm,int M){
	  if(m.getNom().equals("CC")||m.getNom().equals("CW")) return new CR(rho);
	  else if(m.getNom().equals("CR")){
		  int resRho = Connecteurs.tconorm(rho, m.getRho(), tconorm,M);
		  if(resRho==0) return new CC();
		  else return new CR(resRho);
	  }
	  else return null;
  }

  public String toString()
  {return "CR(" + rho + ")";}

}
